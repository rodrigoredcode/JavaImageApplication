import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import javax.swing.*;
import javax.management.InvalidAttributeValueException;



public class MainUI {
	private JFrame mainFrame;
	private JLabel imgBefore;
	private JLabel imgAfter;
	private JFileChooser chooserOpen;
	private JFileChooser chooserSave;
	private JButton btnChooser;
	private JRadioButton btnRed;
	private JRadioButton btnGreen;
	private JRadioButton btnBlue;
	private JRadioButton btnUncover;
	private JRadioButton btnNegative;
	private JRadioButton btnBlur;
	private JRadioButton btnSharpen;
	private JPanel controlPanel;
	
	public MainUI(){
		prepareUI();
	}
	/**
	 * @param args
	 * @throws InvalidAttributeValueException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws InvalidAttributeValueException, IOException {
		String location;
		Scanner in = new Scanner(System.in);
		location = in.next();
		FileInputStream inputStream = new FileInputStream(location);
		ImageReader imgReader = new ImageReader();
		imgReader.read(inputStream);
		ImagePGM imgPGM = new ImagePGM();
		imgPGM.uncoverMessage(inputStream,imgReader.getHideMsgPosition());
		
		System.out.println(imgReader.getType());
		System.out.println(imgReader.getComentary());
		System.out.println(imgReader.getWidth());
		System.out.println(imgReader.getHeight());
		System.out.println(imgReader.getMaxValPixel());
		System.out.println(imgReader.getHideMsgPosition());
	}
	private void prepareUI(){
		mainFrame = new JFrame("Image Steganography and Filters");
		mainFrame.setSize(600, 600);
		mainFrame.setLayout(new GridLayout(1, 3));
		
		imgBefore = new JLabel("",JLabel.CENTER );
		imgAfter = new JLabel("", JLabel.CENTER);
		imgBefore.setSize(300, 200);
		imgAfter.setSize(300,200);
		
	}

}
