
abstract public class Image {
	private String type;
	private String comentary;
	private int width;
	private int height;
	private int maxValPixel;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getComentary() {
		return comentary;
	}
	public void setComentary(String comentary) {
		this.comentary = comentary;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getMaxValPixel() {
		return maxValPixel;
	}
	public void setMaxValPixel(int maxValPixel) {
		this.maxValPixel = maxValPixel;
	}
	
	
}
